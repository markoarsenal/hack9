import { Cities } from 'types';
import { Quality } from 'utils';

export const qualityColors: Record<Quality, { color: string; bgColor: string }> = {
  good: {
    color: '#000',
    bgColor: '#0dad4e',
  },
  moderate: {
    color: '#000',
    bgColor: '#feff11',
  },
  'unhealthy-for-sensitive-group': {
    color: '#000',
    bgColor: '#ffc107',
  },
  unhealthy: {
    color: '#fff',
    bgColor: '#fd070c',
  },
  'very-unhealthy': {
    color: '#fff',
    bgColor: '#6d3798',
  },
  hazardous: {
    color: '#fff',
    bgColor: '#bb080b',
  },
};

export const citiesList: Record<Cities, { name: string; lat: number; lng: number }> = {
  belgrade: {
    name: 'Belgrade',
    lat: 44.787197,
    lng: 20.457273,
  },
  amsterdam: {
    name: 'Amsterdam',
    lat: 52.377956,
    lng: 4.89707,
  },
  novi_sad: {
    name: 'Novi Sad',
    lat: 45.2396,
    lng: 19.8227,
  },
  zrenjanin: {
    name: 'Zrenjanin',
    lat: 45.3834,
    lng: 20.3906,
  },
  kyiv: {
    name: 'Kyiv',
    lat: 50.4501,
    lng: 30.5234,
  },
  iasi: {
    name: 'Iasi',
    lat: 47.1585,
    lng: 27.6014,
  },
};
