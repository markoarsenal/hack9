import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.jedvasesabrasmo.app',
  appName: 'jedvasesabrasmo',
  webDir: 'out',
  bundledWebRuntime: false,
  plugins: {
    LocalNotifications: {
      smallIcon: 'ic_stat_icon_config_sample',
      iconColor: '#0b93f2',
      sound: 'beep.wav',
    },
  },
};

export default config;
