module.exports = {
  content: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  theme: {
    extend: {
      colors: {
        blue: {
          DEFAULT: '#0b93f2',
        },
        green: {
          DEFAULT: '#0dad4e',
        },
        yellow: {
          DEFAULT: '#feff11',
        },
        orange: {
          DEFAULT: '#ffc107',
        },
        purple: {
          DEFAULT: '#6d3798',
        },
        red: {
          DEFAULT: '#fd070c',
          darker: '#bb080b',
        },
      },
    },
  },
  plugins: [],
};
