export type Cities = 'belgrade' | 'novi_sad' | 'zrenjanin' | 'amsterdam' | 'kyiv' | 'iasi';

export type CityData = {
  time: string;
  latitude: string;
  longitude: string;
  location: string;
  name: string;
  pm10: string;
  pm25: string;
  pm100: string;
  temperature: string;
  pressure: string;
  humidity: string;
};
