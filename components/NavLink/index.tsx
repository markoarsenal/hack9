import { AnchorHTMLAttributes, PropsWithChildren, ReactElement } from 'react';
import clsx from 'clsx';
import { useRouter } from 'next/router';
import Link from 'next/link';

type Props = PropsWithChildren<{
  href: string;
  exact?: boolean;
  className?: string;
  activeClassName?: string;
  htmlProps?: AnchorHTMLAttributes<HTMLAnchorElement>;
}>;

const NavLink = ({
  children,
  href,
  exact,
  className = '',
  activeClassName = 'active',
  htmlProps,
}: Props): ReactElement => {
  const { pathname } = useRouter();
  const isActive = exact ? pathname === href : pathname.startsWith(href);

  return (
    <Link href={href}>
      <a className={clsx(className, { [activeClassName]: isActive })} {...htmlProps}>
        {children}
      </a>
    </Link>
  );
};

export default NavLink;
