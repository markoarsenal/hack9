import clsx from 'clsx';
import { ReactNode } from 'react';

type IndexBoxProps = {
  pm10?: number;
  pm25?: number;
  pm100?: number;
  temperature?: number;
  pressure?: number;
  humidity?: number;
  className?: string;
};

type BoxProps = {
  title: ReactNode;
  subtitle: string;
  value: number;
};

const Box = ({ title, subtitle, value }: BoxProps) => {
  return (
    <div>
      <h3 className="text-lg text-black font-bold">{title}</h3>
      <div className="-mt-1 text-xs text-gray-500">{subtitle}</div>
      <h3 className="mt-1 text-lg text-blue font-bold">{value}</h3>
    </div>
  );
};

const IndexBox = ({ pm10, pm25, pm100, temperature, pressure, humidity, className }: IndexBoxProps) => {
  return (
    <div className={clsx('border border-gray-300 rounded bg-gray-50', className)}>
      <ul className="flex w-full">
        <li className="w-1/2 grow-0 shrink-0 px-3 py-1">
          <Box
            title={
              <>
                PM<span className="text-xs">10</span>
              </>
            }
            subtitle="particulate matter"
            value={pm10}
          />
        </li>
        <li className="w-1/2 grow-0 shrink-0 px-3 py-1 border-l border-gray-300">
          <Box
            title={
              <>
                PM<span className="text-xs">2.5</span>
              </>
            }
            subtitle="particulate matter"
            value={pm25}
          />
        </li>
      </ul>
      <ul className="flex w-full border-t border-gray-300">
        <li className="w-1/2 grow-0 shrink-0 px-3 py-1">
          <Box
            title={
              <>
                PM<span className="text-xs">100</span>
              </>
            }
            subtitle="particulate matter"
            value={pm100}
          />
        </li>
        <li className="w-1/2 grow-0 shrink-0 px-3 py-1 border-l border-gray-300">
          <Box title="Temperature" subtitle="°C" value={temperature} />
        </li>
      </ul>
      <ul className="flex w-full border-t border-gray-300">
        <li className="w-1/2 grow-0 shrink-0 px-3 py-1">
          <Box title="Humidity" subtitle="g/m3" value={humidity} />
        </li>
        <li className="w-1/2 grow-0 shrink-0 px-3 py-1 border-l border-gray-300">
          <Box title="Pressure" subtitle="millibars" value={pressure} />
        </li>
      </ul>
    </div>
  );
};

export default IndexBox;
