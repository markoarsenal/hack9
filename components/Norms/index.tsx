type NormsProps = {
  className?: string;
};

const Norms = ({ className }: NormsProps) => {
  return (
    <div className={className}>
      <h3 className="mb-2">
        Norms <span className="font-semibold">μg/m3</span>
      </h3>
      <ul className="flex">
        <li className="w-1/6 grow-0 shrink-0">
          <ul className="flex flex-col">
            <li style={{ fontSize: '10px' }}>0</li>
            <li className="h-2 mb-0.5 rounded-l bg-green"></li>
            <li style={{ fontSize: '8px' }}>Good</li>
          </ul>
        </li>
        <li className="w-1/6 grow-0 shrink-0 pl-px">
          <ul className="flex flex-col">
            <li style={{ fontSize: '10px' }}>50</li>
            <li className="h-2 mb-0.5 bg-yellow"></li>
            <li style={{ fontSize: '8px' }}>Moderate</li>
          </ul>
        </li>
        <li className="w-1/6 grow-0 shrink-0 pl-px">
          <ul className="flex flex-col">
            <li style={{ fontSize: '10px' }}>100</li>
            <li className="h-2 mb-0.5 bg-orange"></li>
            <li style={{ fontSize: '8px' }}>Unhealthy for sensitive group</li>
          </ul>
        </li>
        <li className="w-1/6 grow-0 shrink-0 pl-px">
          <ul className="flex flex-col">
            <li style={{ fontSize: '10px' }}>200</li>
            <li className="h-2 mb-0.5 bg-red"></li>
            <li style={{ fontSize: '8px' }}>Unhealthy</li>
          </ul>
        </li>
        <li className="w-1/6 grow-0 shrink-0 pl-px">
          <ul className="flex flex-col">
            <li style={{ fontSize: '10px' }}>300</li>
            <li className="h-2 mb-0.5 bg-purple"></li>
            <li style={{ fontSize: '8px' }}>Very unhealthy</li>
          </ul>
        </li>
        <li className="w-1/6 grow-0 shrink-0 pl-px">
          <ul className="flex flex-col">
            <li className="flex justify-between" style={{ fontSize: '10px' }}>
              400 <span>∞</span>
            </li>
            <li className="h-2 mb-0.5 rounded-r bg-red-darker"></li>
            <li style={{ fontSize: '8px' }}>Hazardous</li>
          </ul>
        </li>
      </ul>
    </div>
  );
};

export default Norms;
