import { PropsWithChildren } from 'react';
import clsx from 'clsx';
import NavLink from 'components/NavLink';
import MapIcon from 'assets/icons/location.svg';
import ListIcon from 'assets/icons/list.svg';
import WalkIcon from 'assets/icons/directions_walk.svg';
import NotificationIcon from 'assets/icons/notifications.svg';
import styles from './layout.module.css';

const Layout = ({ children }: PropsWithChildren<{}>) => {
  return (
    <div className="page-container">
      <div className="grow flex justify-center items-center">{children}</div>
      <ul className={clsx('flex justify-center sticky bottom-0 px-4 py-2 -mx-6 bg-white', styles.footer)}>
        <li>
          <NavLink
            href="/"
            exact
            className="inline-flex justify-center px-2 py-1 text-gray-700 rounded fill-current hover:text-white hover:bg-blue"
            activeClassName={clsx('bg-blue', styles['active-link'])}
          >
            <MapIcon width={18} className="mr-2" />
            Map
          </NavLink>
        </li>
        <li className="ml-2">
          <NavLink
            href="/cities"
            className="inline-flex justify-center px-2 py-1 text-gray-700 rounded fill-current hover:text-white hover:bg-blue"
            activeClassName={clsx('bg-blue', styles['active-link'])}
          >
            <ListIcon width={18} className="mr-2" />
            Cities
          </NavLink>
        </li>
        <li className="ml-2">
          <NavLink
            href="/walk"
            className="inline-flex justify-center px-2 py-1 text-gray-700 rounded fill-current hover:text-white hover:bg-blue"
            activeClassName={clsx('bg-blue', styles['active-link'])}
          >
            <WalkIcon width={18} className="mr-2" />
            Walk
          </NavLink>
        </li>
        <li className="ml-2">
          <NavLink
            href="/notify"
            className="inline-flex justify-center px-2 py-1 text-gray-700 rounded fill-current hover:text-white hover:bg-blue"
            activeClassName={clsx('bg-blue', styles['active-link'])}
          >
            <NotificationIcon width={18} className="mr-2" />
            Notify
          </NavLink>
        </li>
      </ul>
    </div>
  );
};

export default Layout;
