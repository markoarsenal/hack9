import { Line } from 'react-chartjs-2';
import { format } from 'date-fns';

import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from 'chart.js';
import { getQualityIndex } from '../../utils';
import clsx from 'clsx';

ChartJS.register(CategoryScale, LinearScale, PointElement, LineElement, Title, Tooltip, Legend);

type DaysHoursChartProps = {
  data: Record<string, { pm10: string, pm25: string }>;
  className?: string
}

const DaysHoursChart = ({ data, className }: DaysHoursChartProps) => {
  const last7Days = [];
  const data8 = [];
  const data14 = [];
  const data20 = [];

  Object.entries(data).map(([key, val]) => {
    last7Days.push(key);
    last7Days.sort();
    data8.push(getQualityIndex({ pm10: Number(val[8].pm10), pm25: Number(val[8].pm25) }));
    data14.push(getQualityIndex({ pm10: Number(val[14].pm10), pm25: Number(val[14].pm25) }));
    data20.push(getQualityIndex({ pm10: Number(val[20].pm10), pm25: Number(val[20].pm25) }));
  });

  const labels = last7Days.map((date) => formatDate(date) + ' (' + formatWeekDay(date) + ')');

  return (
    <div className={clsx('w-full', className)}>
      <Line
        datasetIdKey='id'
        options={{ elements: {
          line: { tension: 0.5 }
          }}}
        data={{
          labels: labels,
          datasets: [
            {
              label: '08 AM',
              data: data8,
              borderColor: '#f7c77c',
              backgroundColor: '#f7c77c',
              borderWidth: 1,
            },
            {
              label: '14 PM',
              data: data14,
              borderColor: '#51c3c4',
              backgroundColor: '#51c3c4',
              borderWidth: 1,
            },
            {
              label: '20 PM',
              data: data20,
              borderColor: '#2564f2',
              backgroundColor: '#2564f2',
              borderWidth: 1,
            },
          ],
        }}
      />
    </div>
  );
};

const formatDate = (date): string => {
  const day = date.slice(0, 2);
  const month = date.slice(3, 5);
  return day + '/' + month;
};

const formatWeekDay = (date): string => {
  const year = Number(date.slice(6, 10));
  const month = Number(date.slice(3, 5));
  const day = Number(date.slice(0, 2));
  return format(new Date(year, month, day), 'E');
};

export default DaysHoursChart;
