import { useEffect } from 'react';
import { useRouter } from 'next/router';

type CircleProps = {
  city: string;
  lat?: number;
  lng?: number;
  color?: string;
  map?: any;
  maps?: any;
};

const Circle = ({ city, color, lat, lng, map, maps }: CircleProps) => {
  const router = useRouter();

  useEffect(() => {
    const cityCircle = new maps.Circle({
      strokeColor: color,
      strokeOpacity: 0.4,
      strokeWeight: 20,
      fillColor: color,
      fillOpacity: 0.8,
      map,
      center: { lat, lng },
      radius: 8000,
    });

    cityCircle.addListener('click', () => router.push('/cities/' + city));
  }, [city, color, lat, lng, map, maps.Circle, router]);

  return <div className="w-full h-full"></div>;
};

export default Circle;
