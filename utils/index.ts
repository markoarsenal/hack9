import aqiCalculator from 'aqi-calculator';

export type Quality =
  | 'good'
  | 'moderate'
  | 'unhealthy-for-sensitive-group'
  | 'unhealthy'
  | 'very-unhealthy'
  | 'hazardous';

export const getQuality = (data: { pm10: number; pm25: number }): Quality => {
  const quality = aqiCalculator([data]);

  switch (true) {
    case quality <= 50:
      return 'good';

    case quality > 50 && quality <= 100:
      return 'moderate';

    case quality > 100 && quality <= 200:
      return 'unhealthy-for-sensitive-group';

    case quality > 200 && quality <= 300:
      return 'unhealthy';

    case quality > 300 && quality <= 400:
      return 'very-unhealthy';

    default:
      return 'hazardous';
  }
};

export const getQualityIndex = (data: { pm10: number; pm25: number }): number => aqiCalculator([data]);
