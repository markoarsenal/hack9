import { LocalNotifications } from '@capacitor/local-notifications';
import { useEffect, useRef, useState } from 'react';
import ReactSelect from 'react-select';
import useSWR from 'swr';
import { Cities, CityData } from 'types';
import { getQualityIndex } from 'utils';

const Notify = () => {
  const [start, setStart] = useState(false);
  const [value, setValue] = useState(null);

  const { data } = useSWR<Record<Cities, CityData>, Error>(
    'https://8zdqfsnlk1.execute-api.eu-central-1.amazonaws.com/prod/locations',
  );

  const interval = useRef(null);

  useEffect(() => {
    const notify = async () => {
      await LocalNotifications.requestPermissions();
      await LocalNotifications.schedule({
        notifications: [
          {
            id: 0,
            title: 'Air quality',
            body: `${getQualityIndex({
              pm10: parseInt(data['belgrade'].pm10 || '0'),
              pm25: parseInt(data['belgrade'].pm25 || '0'),
            })}`,
            iconColor: '#0b93f2',
          },
        ],
      });
    };

    interval.current && clearInterval(interval.current);
    interval.current = start && value && value.value ? setInterval(notify, 60 * 1000 * value.value) : null;

    return () => clearInterval(interval.current);
  }, [data, start, value]);

  useEffect(() => {
    localStorage.getItem('notify') && setValue(JSON.parse(localStorage.getItem('notify')));
  }, []);

  return (
    <div className="w-full">
      <h1 className="w-full mb-4 text-left text-lg font-semibold">Notifications</h1>
      <div className="mb-0.5">Choose notifications timing</div>
      <ReactSelect
        placeholder="Choose..."
        isSearchable={false}
        options={[
          { label: '10 seconds', value: 0.1667 },
          { label: '1 minute', value: 1 },
          { label: '1 hour', value: 60 },
          { label: '1 day', value: 24 * 60 },
          { label: '1 week', value: 24 * 60 * 7 },
          { label: 'Never', value: 0 },
        ]}
        value={value}
        onChange={(val) => setValue(val)}
      />
      <button
        className="block w-full py-2 mt-3 rounded text-white bg-blue hover:bg-opacity-90"
        onClick={() => {
          value && localStorage.setItem('notify', JSON.stringify(value));
          setStart(true);
        }}
      >
        Save
      </button>
    </div>
  );
};

export default Notify;
