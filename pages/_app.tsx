import Head from 'next/head';
import Layout from 'components/Layout';
import '../styles/globals.css';
import { SWRConfig } from 'swr';
import { useEffect } from 'react';

export type FetcherData = {
  body: Record<string, unknown>;
  method: string;
};

export const fetcher = () => async (path: string, data?: FetcherData) => {
  const res = await fetch(path, {
    method: data?.method,
    body: JSON.stringify(data?.body),
  });

  if (!res.ok) throw new Error('error');

  return res.json();
};

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Head>
        <meta
          name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, viewport-fit=cover"
        />
        <meta name="apple-mobile-web-app-capable" content="yes"></meta>
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"></meta>
      </Head>
      <Layout>
        <SWRConfig
          value={{
            revalidateOnFocus: false,
            fetcher: fetcher(),
          }}
        >
          <Component {...pageProps} />
        </SWRConfig>
      </Layout>
    </>
  );
}

export default MyApp;
