import React, { useEffect, useMemo, useState } from 'react';
import GoogleMapReact from 'google-map-react';
import { Geolocation } from '@capacitor/geolocation';
import useSWR from 'swr';
import Circle from 'components/Circle';
import { Cities, CityData } from '../types';
import { getQuality, getQualityIndex } from '../utils';
import { citiesList, qualityColors } from '../config';

const Home = () => {
  const [center, setCenter] = useState({ lat: 47.1585, lng: 27.6014 });
  const [zoom, setZoom] = useState(10);
  const [cities, setCities] = useState<Partial<Record<Cities, { color: string; lat: number; lng: number }>>>(null);
  const [map, setMap] = useState(null);
  const [maps, setMaps] = useState(null);

  const { data } = useSWR<Record<Cities, CityData>, Error>(
    'https://8zdqfsnlk1.execute-api.eu-central-1.amazonaws.com/prod/locations',
  );

  const filteredCities = useMemo(
    () =>
      Object.entries(citiesList).filter(([key]) => {
        return !!data?.[key];
      }),
    [data],
  );

  useEffect(() => {
    setCities(
      filteredCities.reduce((acc, [key, val]) => {
        return {
          ...acc,
          [key]: {
            ...val,
            color:
              qualityColors[
                getQuality({ pm10: parseInt(data[key].pm10 || '0'), pm25: parseInt(data[key].pm25 || '0') })
              ].bgColor,
          },
        };
      }, {}),
    );
  }, [data, filteredCities]);

  const onGoogleApiLoaded = async ({ map, maps }) => {
    setMap(map);
    setMaps(maps);

    const data = await Geolocation.getCurrentPosition();

    if (data)
      setCenter({
        lat: data.coords.latitude,
        lng: data.coords.longitude,
      });
  };

  return (
    <div className="w-full h-full">
      <div className="h-full -ml-6" style={{ width: 'calc(100% + 48px)' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyA4BZk1GUyjDX0LGKWNom-s2SQCxjtXgD0' }}
          center={center}
          zoom={zoom}
          yesIWantToUseGoogleMapApiInternals
          onGoogleApiLoaded={onGoogleApiLoaded}
          onChange={({ center, zoom }) => {
            localStorage.setItem('center', JSON.stringify(center));
            localStorage.setItem('zoom', JSON.stringify(zoom));
          }}
        >
          {map &&
            maps &&
            cities &&
            Object.entries(cities).map(([key, { lat, lng, color }]) => (
              <Circle key={key} city={key} lat={lat} lng={lng} color={color} map={map} maps={maps} />
            ))}
        </GoogleMapReact>
      </div>
    </div>
  );
};

export default Home;
