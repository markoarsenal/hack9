import { citiesList, qualityColors } from 'config';
import { useRouter } from 'next/router';
import useSWR from 'swr';
import { Cities, CityData } from 'types';
import { getQuality, getQualityIndex } from 'utils';
import Norms from 'components/Norms';
import IndexBox from 'components/IndexBox';
import Chart from '../../components/DaysHoursChart';

const City = () => {
  const { query } = useRouter();
  const { data } = useSWR<Record<Cities, CityData>, Error>(
    'https://8zdqfsnlk1.execute-api.eu-central-1.amazonaws.com/prod/locations',
  );

  console.log('name',query.name);

  const { data: cityDataForChart } = useSWR(
    query.name ? 'https://8zdqfsnlk1.execute-api.eu-central-1.amazonaws.com/prod/last-week?location=' + query.name : null,
  );
  const cityData = data ? data[query.name as Cities] : null;
  const indexes = cityData
    ? {
        pm10: parseInt(cityData.pm10 || '0'),
        pm25: parseInt(cityData.pm25 || '0'),
        pm100: parseInt(cityData.pm100 || '0'),
        temperature: Number(parseFloat(cityData.temperature || '0').toFixed(2)),
        pressure: Number(parseFloat(cityData.pressure || '0').toFixed(2)),
        humidity: parseInt(cityData.humidity || '0'),
      }
    : null;
  const pm = indexes ? { pm10: indexes.pm10, pm25: indexes.pm25 } : null;
  const q = pm ? getQuality(pm) : null;
  const qn = pm ? getQualityIndex(pm) : null;

  return query.name ? (
    <div className="w-full h-full">
      <div
        className="h-32 -mx-6 flex justify-center items-center bg-cover bg-center"
        style={{ backgroundImage: `url(/images/${query.name}.jpeg)` }}
      >
        <h1 className="px-6 py-1 border border-white rounded-sm text-lg text-gray-800 font-semibold bg-white bg-opacity-80">
          {citiesList[query.name as Cities].name}
        </h1>
      </div>
      {q && (
        <div
          className="h-8 px-4 -mx-6 flex items-center"
          style={{ color: qualityColors[q].color, backgroundColor: qualityColors[q].bgColor }}
        >
          air quality: {qn || 'n/a'}
          <span className="ml-2 text-xs">({q.replace(/-/g, ' ')})</span>
        </div>
      )}
      <Norms className="mt-4" />
      {indexes && <IndexBox className="mt-8" {...indexes} />}
      {cityDataForChart && <Chart className="mt-8 pb-8" data={cityDataForChart.sensor_data} />}
    </div>
  ) : null;
};

export default City;
