import { useMemo } from 'react';
import Link from 'next/link';
import clsx from 'clsx';
import useSWR from 'swr';
import { citiesList, qualityColors } from 'config';
import { getQuality, getQualityIndex } from 'utils';
import Norms from 'components/Norms';
import { Cities, CityData } from 'types';

const CitiesPage = () => {
  const { data } = useSWR<Record<Cities, CityData>, Error>(
    'https://8zdqfsnlk1.execute-api.eu-central-1.amazonaws.com/prod/locations',
  );
  const sortedAndFilteredCities = useMemo(
    () =>
      Object.entries(citiesList)
        .sort((a, b) => {
          const cityDataA = data ? data[a[0]] : null;
          const indexesA = cityDataA
            ? {
                pm10: parseInt(cityDataA.pm10 || '0'),
                pm25: parseInt(cityDataA.pm25 || '0'),
              }
            : null;
          const pmA = indexesA ? { pm10: indexesA.pm10, pm25: indexesA.pm25 } : null;
          const qnA = pmA ? getQualityIndex(pmA) : null;
          const cityDataB = data ? data[b[0]] : null;
          const indexesB = cityDataB
            ? {
                pm10: parseInt(cityDataB.pm10 || '0'),
                pm25: parseInt(cityDataB.pm25 || '0'),
              }
            : null;
          const pmB = indexesB ? { pm10: indexesB.pm10, pm25: indexesB.pm25 } : null;
          const qnB = pmB ? getQualityIndex(pmB) : null;

          return qnA < qnB ? -1 : 1;
        })
        .filter(([key]) => {
          return !!data?.[key];
        }),
    [data],
  );

  return (
    <div className="w-full flex flex-col items-center">
      <h1 className="w-full mb-8 text-left text-lg font-semibold">Cities air quality list</h1>
      <Norms className="w-full mb-8" />
      <ul className="w-full rounded-md overflow-hidden">
        {sortedAndFilteredCities.map(([key, { name }], i) => {
          const cityData = data ? data[key] : null;
          const indexes = cityData
            ? {
                pm10: parseInt(cityData.pm10 || '0'),
                pm25: parseInt(cityData.pm25 || '0'),
                pm100: parseInt(cityData.pm100 || '0'),
                temperature: Number(parseFloat(cityData.temperature || '0').toFixed(2)),
                pressure: Number(parseFloat(cityData.pressure || '0').toFixed(2)),
                humidity: parseInt(cityData.humidity || '0'),
              }
            : null;
          const pm = indexes ? { pm10: indexes.pm10, pm25: indexes.pm25 } : null;
          const q = pm ? getQuality(pm) : null;
          const qn = pm ? getQualityIndex(pm) : null;

          return q ? (
            <li key={key} className={clsx('flex', i !== 0 && 'border-t-4 border-white')}>
              <div className="flex shrink-0 grow-0 justify-center items-center w-8 bg-blue">{i + 1}</div>
              <Link href={`/cities/${key}`}>
                <a
                  className="block grow px-2 py-1.5 text-white"
                  style={{
                    color: qualityColors[q].color,
                    backgroundColor: qualityColors[q].bgColor,
                  }}
                >
                  {name} - <span className="font-semibold">{qn}</span>{' '}
                  <span className="text-xs truncate">({q.replace(/-/g, ' ')})</span>
                </a>
              </Link>
            </li>
          ) : null;
        })}
      </ul>
    </div>
  );
};

export default CitiesPage;
